<?php
   function range_check( $num ,$min,$max ,$include_same =true){
       if( $include_same){
           if( $min <= $num && $num <= $max){ return true; }
       }
       else{
           if( $min < $num && $num < $max){  return true;  }
       }
       return false;
    }
    $input_lines = fgets(STDIN);
    if( !range_check($input_lines,1,100) ){
        exit;
    }
    list($t,$n) = preg_split("/\s/",$input_lines);
    if( !range_check($t,1,300000) ){ exit; }
    if( !range_check($n,1,300000) ){ exit; }
    if( !range_check($t,1,$n) ){ exit; }
     $nums_array = array();
     for ( $i = 0; $i < $n; $i++) {
      $s = trim(fgets(STDIN));

       if( !range_check($s,0,10000) ){ exit; }
        $nums_array[] = $s;
     }
     $max = 0;
     $total =array();
     $total[0] = 0;
    $max = 0;
    for ( $i = 0; $i < ($n); $i++) {
        $num = $nums_array[$i];

        $total[$i] = $total[($i-1)] + $num;
        $aaa  = $total[ $i ] - $total[ ($i -$t) ];
        if( $aaa >$max){ $max =$aaa;}
    }
    echo $max;
