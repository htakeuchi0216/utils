<?php
    // 自分の得意な言語で
    // Let's チャレンジ！！

   function range_check( $num ,$min,$max ,$include_same =true){
       if( $include_same){
           if( $min <= $num && $num <= $max){
	       return true;
           }
       }
       else{
           if( $min < $num && $num < $max){
	       return true;
           }
       }
       return false;
    }



    $input_lines = fgets(STDIN);
    if( !range_check($input_lines,1,100) ){
        exit;
    }

    $sum = 0;
    for ( $i = 0; $i < $input_lines; $i++) {
      $s = trim(fgets(STDIN));
      $line = preg_split("/\s/",$s);

      $need   = $line[0];
      $zaiko  = $line[1];
      $price  = $line[2];
      if(
        !range_check($need,1,10000)
      || !range_check($zaiko,0,10000)
      || !range_check($price,1,10000)
      ){
           continue;
      }

#      print "need:{$need} , zaiko:{$zaiko} , price: {$price} \n";
      if($need > $zaiko){
          $sum += ($need-$zaiko) *$price;
      }
      
    }
      echo $sum;



